#include <QGuiApplication>
#include <QtQuick/QQuickView>
#include <QtWebEngine>

Q_DECL_EXPORT int main(int argc, char *argv[]) {
	QGuiApplication app(argc, argv);
	QtWebEngine::initialize();

	QQuickView viewer(QUrl("qrc:/main.qml"));
	viewer.showFullScreen();

	return app.exec();
}
