import QtQuick 2.0
import QtWebEngine 1.3

Rectangle {
	id: rootItem
	property WebEngineView webView

	anchors.fill: parent
	color: "#C9F8FE"

	Text {
		anchors.centerIn: parent
		text: "bitte würfeln"
		font.pixelSize: 100
		color: "#333333"
		focus: false
	}

	function loadURL(u) {
		if (rootItem.webView) rootItem.webView.destroy()
		rootItem.webView = webComponent.createObject(rootItem)
		rootItem.webView.url = u
	}

	Component.onCompleted: rootItem.forceActiveFocus()

	Keys.onPressed: {
		if (event.modifiers & Qt.ControlModifier) {
			switch (event.key) {
				case Qt.Key_0: if (rootItem.webView) {
					rootItem.webView.destroy()
					rootItem.forceActiveFocus()
				}
				break;
				case Qt.Key_1: rootItem.loadURL("http://quiz.zeit.de/#/quiz/1239")
				break;
				case Qt.Key_2: rootItem.loadURL("http://quiz.zeit.de/#/quiz/1246")
				break;
				case Qt.Key_3: rootItem.loadURL("http://quiz.zeit.de/#/quiz/1248")
				break;
				case Qt.Key_4: rootItem.loadURL("http://quiz.zeit.de/#/quiz/1258")
				break;
				case Qt.Key_5: rootItem.loadURL("http://quiz.zeit.de/#/quiz/1269")
				break;
				case Qt.Key_6: rootItem.loadURL("http://quiz.zeit.de/#/quiz/1280")
				break;
			}
		}
		event.accepted = true
	}

	property Component webComponent: WebEngineView {
		anchors.fill: parent
		profile: WebEngineProfile {
			offTheRecord: true
		}
	}
}
